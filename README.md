Macedonian businesses 

Application content:
-	Data visualization 
-	Data preprocessing 
-	Defining Data clusters 
-	Using clusters for data classification 
-	Classification with SVC
-	Classification with Random Forest

Data content explanation:
-	Business type example:
Type 1 - Pharmaceutical industry, 
Type 2 - Construction industry,  
Type 3 – Manufacturing, Type 4 – Transportation and forwarding, Type 5 – Financial industry, 
….

-	Business size:
Refers to employed people  
-	Stock loss (Jan):
Refers to stock price difference from January 2020 (Stable period) to last month stock price
-	Stock loss AVG:
Refers to Average stock price loss for period of January 2020 to last stock price
-	Stock loss LM:
Refers to last month stock prices compared to the previous month 
